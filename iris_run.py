import os
import mlflow
import requests
import pandas as pd
import numpy as np
import mlflow.sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn import datasets

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://95.216.191.176:9001"
os.environ["MLFLOW_TRACKING_URI"] = "http://95.216.191.176:9000"
os.environ["AWS_ACCESS_KEY_ID"] = "IAM_ACCESS_KEY"
os.environ["AWS_SECRET_ACCESS_KEY"] = "IAM_SECRET_KEY"

mlflow.set_tracking_uri("http://95.216.191.176:9000")
client = mlflow.tracking.MlflowClient()

experiment = client.get_experiment_by_name("iris_sklearn")

