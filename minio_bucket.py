from minio import Minio

accessID = "IAM_ACCESS_KEY"
accessSecret = "IAM_SECRET_KEY"
minioUrl = "http://95.216.191.176:9000"
bucketName = "mlflow"

minioUrlHostWithPort = minioUrl.split("//")[1]
print(minioUrlHostWithPort)
s3Client = Minio(minioUrlHostWithPort, access_key=accessID, secret_key=accessSecret, secure=False)

s3Client.make_bucket(bucketName)

